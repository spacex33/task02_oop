package exception;

/**
 * Signals that instance is not found in some range.
 *
 * @author Sergey
 * @since 2018-11-16
 */
public class NotFoundInRangeException extends RuntimeException {

    /**
     * Constructs a NotFoundInRangeException with the specified
     * detail message. A detail message is a String that describes
     * this particular exception.
     *
     * @param message the detail message.
     */
    public NotFoundInRangeException(String message) {
        super(message);
    }
}
