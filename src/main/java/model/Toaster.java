package model;

public class Toaster extends ElectricalAppliance {

    private int numberOfToasts;
    private boolean adjustingDegreeOfRoasting;

    public Toaster(String name, int powerConsumption, int numberOfToasts, boolean adjustingDegreeOfRoasting) {
        super(name, powerConsumption);
        this.numberOfToasts = numberOfToasts;
        this.adjustingDegreeOfRoasting = adjustingDegreeOfRoasting;
    }

    public int getNumberOfToasts() {
        return numberOfToasts;
    }

    public boolean isAdjustingDegreeOfRoasting() {
        return adjustingDegreeOfRoasting;
    }

    @Override
    public String toString() {
        return super.toString()
                + "number of toasts = " + this.numberOfToasts
                + ", adjusting degree of roasting " + this.adjustingDegreeOfRoasting;
    }
}
