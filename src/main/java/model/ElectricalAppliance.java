package model;

public abstract class ElectricalAppliance {

    private String name;
    private int powerConsumption;
    private boolean isTurnedOn;

    public ElectricalAppliance(String name, int powerConsumption) {
        this.name = name;
        this.powerConsumption = powerConsumption;
    }

    public boolean plugIntoOutlet() {
        return this.isTurnedOn = true;
    }

    public int getPowerConsumption() {
        return powerConsumption;
    }

    public boolean isTurnedOn() {
        return isTurnedOn;
    }

    @Override
    public String toString() {
        return "name = '" + name + '\'' +
                ", power consumption = " + powerConsumption +
                ", turned on = " + isTurnedOn;
    }
}
