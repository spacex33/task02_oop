package model;

public class Multicook extends ElectricalAppliance {

    private float volumeCapacity;
    private int numberOfPrograms;

    public Multicook(String name, int powerConsumption, float volumeCapacity, int numberOfPrograms) {
        super(name, powerConsumption);
        this.volumeCapacity = volumeCapacity;
        this.numberOfPrograms = numberOfPrograms;
    }

    public float getVolumeCapacity() {
        return volumeCapacity;
    }

    public int getNumberOfPrograms() {
        return numberOfPrograms;
    }

    @Override
    public String toString() {
        return super.toString()
                + ", volume capacity = " + this.volumeCapacity
                + ", number of programs " + this.numberOfPrograms;
    }
}
