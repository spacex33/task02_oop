package model;

public class HairDryer extends ElectricalAppliance {

    private boolean isThermostatAvailable;
    private int numberOfNozzles;

    public HairDryer(String name, int powerConsumption, boolean isThermostatAvailable, int numberOfNozzles) {
        super(name, powerConsumption);
        this.isThermostatAvailable = isThermostatAvailable;
        this.numberOfNozzles = numberOfNozzles;
    }

    public boolean isThermostatAvailable() {
        return isThermostatAvailable;
    }

    public int getNumberOfNozzles() {
        return numberOfNozzles;
    }

    @Override
    public String toString() {
        return super.toString()
                + ", thermostat = " + this.isThermostatAvailable
                + ", number of nozzles " + this.numberOfNozzles;
    }
}
