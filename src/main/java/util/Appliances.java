package util;

import exception.NotFoundInRangeException;
import model.ElectricalAppliance;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Appliances {

    public static int actualPowerConsumption(List<ElectricalAppliance> electricalAppliances) {
        return electricalAppliances.stream()
                .filter(ElectricalAppliance::isTurnedOn)
                .mapToInt(ElectricalAppliance::getPowerConsumption)
                .sum();
    }

    public static List<ElectricalAppliance> sortByPower(List<ElectricalAppliance> electricalAppliances) {
        return electricalAppliances.stream()
                .sorted(Comparator.comparingInt(ElectricalAppliance::getPowerConsumption).reversed())
                .collect(Collectors.toList());
    }

    public static ElectricalAppliance findFromRange(
            List<ElectricalAppliance> electricalAppliances,
            int from,
            int to
    ) {
        return Appliances.sortByPower(electricalAppliances)
                .stream().filter(e -> e.getPowerConsumption() >= from)
                .filter(e -> e.getPowerConsumption() <= to)
                .findFirst()
                .orElseThrow(() -> new NotFoundInRangeException("Electrical appliance not found"));
    }


}
