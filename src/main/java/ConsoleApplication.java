import exception.NotFoundInRangeException;
import io.Console;
import model.ElectricalAppliance;
import model.HairDryer;
import model.Multicook;
import model.Toaster;
import util.Appliances;

import java.util.ArrayList;
import java.util.List;

/**
 * Class is responsible for the base logic
 * of interaction between user view and computations.
 *
 * @author Sergey
 * @since 2018-11-14
 */
public class ConsoleApplication {

    /**
     * Instance variable represents interaction with user.
     * The way of this interaction encapsulated inside class.
     *
     * @see Console
     */
    private Console console = new Console();

    /**
     * Instance variable represents list of base classes,
     * which can store all child instances.
     */
    private List<ElectricalAppliance> appliances = new ArrayList<>();

    /**
     * Method used as entry point to console interaction,
     * it maintains application in infinity loop
     * and includes main functionality.
     */
    public final void start() {
        fillList(this.appliances);

        while (true) {

            showConsoleMenu();
            int menuChoice = console.getInput();

            switch (menuChoice) {
                case 1:
                    displayElectricalAppliances(this.appliances);
                    break;
                case 2:
                    int deviceId = askForInput(
                            "Type id of device you want to plug into outlet: ");
                    plugIntoOutlet(deviceId);
                    break;
                case 3:
                    displayPowerConsumption(this.appliances);
                    break;
                case 4:
                    displaySortedElectricalAppliances(this.appliances);
                    break;
                case 5:
                    int from = askForInput("Type from: ");
                    int to = askForInput("Type to: ");
                    displayFoundInRange(this.appliances, from, to);
                    break;
                case 0:
                default:
                    System.exit(0);
            }
        }
    }

    /**
     * Displays first found instance in given list, based on given range.
     *
     * @param appliances list which should be searched
     * @param from       first param of range
     * @param to         second param of range
     */
    private void displayFoundInRange(
            final List<ElectricalAppliance> appliances,
            int from,
            int to
    ) {
        try {
            ElectricalAppliance electricalAppliance =
                    Appliances.findFromRange(appliances, from, to);
            this.console.print(electricalAppliance.toString());

        } catch (NotFoundInRangeException e) {
            this.console.print(e.getMessage());
        }
    }

    /**
     * Displays sorted list based on given list on instances.
     *
     * @param appliances list which should be sorted
     */
    private void displaySortedElectricalAppliances(
            final List<ElectricalAppliance> appliances
    ) {
        Appliances.sortByPower(appliances)
                .forEach(e -> this.console.print(e.toString()));
    }

    /**
     * Displays summation of all power consumption in given list.
     *
     * @param electricalAppliances list in which power consumption must be summed
     */
    private void displayPowerConsumption(
            final List<ElectricalAppliance> electricalAppliances) {
        this.console.print(Appliances.actualPowerConsumption(electricalAppliances));
    }

    /**
     * Plugs into outlet device based on given id.
     *
     * @param device id
     */
    private void plugIntoOutlet(final int device) {
        this.appliances.get(device - 1).plugIntoOutlet();
    }

    /**
     * Displays list by numbered list.
     *
     * @param electricalAppliances list which should be displayed
     */
    private void displayElectricalAppliances(
            final List<ElectricalAppliance> electricalAppliances) {
        int i = 0;
        for (ElectricalAppliance appliance : electricalAppliances) {
            this.console.print(++i + ". " + appliance.toString());
        }
    }

    /**
     * Fills given list.
     * (maybe such logic should look better in another directory, e.g. /test)
     *
     * @param appliances list which should be filled by instances
     */
    private void fillList(final List<ElectricalAppliance> appliances) {
        appliances.add(new HairDryer(
                "Hair Dryer BaByliss PRO BAB6510IE",
                2400,
                true,
                3));
        appliances.add(new HairDryer(
                "Hair Dryer Wahl Super Dry",
                2000,
                true,
                2));
        appliances.add(new Toaster(
                "Toaster PROFI COOK PC-TА 1082",
                850,
                2,
                true));
        appliances.add(new Toaster(
                "Toaster Tristar",
                800,
                2,
                true));
        appliances.add(new Multicook(
                "Multicook Domotec 7711",
                1000,
                5.0f,
                6));

    }

    /**
     * Method used to get user input based on particular request.
     *
     * @param request text that will be displayed to user
     * @return user input
     */
    private int askForInput(final String request) {
        console.print(request);
        return console.getInput();
    }

    /**
     * Method displays menu text.
     */
    private void showConsoleMenu() {
        console.print("1 - Print electrical appliances, "
                + "2 - Plug into outlet, "
                + "3 - Current power consumption, "
                + "4 - Sort by power, "
                + "5 - Search, "
                + "0 - exit");
    }
}
